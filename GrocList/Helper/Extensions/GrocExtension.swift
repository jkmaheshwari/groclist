//
//  GrocExtension.swift
//  GrocList
//
//  Created by Jatesh Kumar on 11/03/2021.
//

import UIKit
import Firebase

extension GrocViewController: UITableViewDelegate, DataAdditionDelegate, UITextFieldDelegate, GrocViewViewProtocol {
    /****TABLE VIEW**/
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 2 {
            return 40.0
        } else {
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeaderLabel = UILabel()
        sectionHeaderLabel.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        sectionHeaderLabel.textAlignment = .center
        sectionHeaderLabel.textColor = .white
        sectionHeaderLabel.font = sectionHeaderLabel.font.withSize(30)
        if section == 0 {
            sectionHeaderLabel.text = "TODO"
        } else if section == 2 {
            sectionHeaderLabel.text = "DONE"
        } else {
            return UILabel()
        }
        return sectionHeaderLabel
    }
    
    func createDatasource () {
        datasource = GrocDataSource(tableView: grocTableView, cellProvider: { (tableView, indexPath, grocs) in
            
            if indexPath.section == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "GrocNameCell", for: indexPath) as? GrocNameCell else { return UITableViewCell() }
                cell.configure(grocData: grocs)
                guard let uid = grocs.grocAssigneeID else { return UITableViewCell()}
                self.presenter?.getAssigneeName(uid: uid)
                cell.assigneeName.text = self.assigneeName
                return cell
            } else if indexPath.section == 1 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "GrocTextFieldCell", for: indexPath) as? GrocTextFieldCell else { return UITableViewCell()}
                cell.delegate = self
                cell.textFieldGroc.delegate = self
                self.cellStatus = true
                return cell
                
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "GrocNameCell", for: indexPath) as? GrocNameCell else {return UITableViewCell()}
                cell.configure(grocData: grocs)
                guard let uid = grocs.grocAssigneeID else { return UITableViewCell()}
                self.presenter?.getAssigneeName(uid: uid)
                cell.assigneeName.text = self.assigneeName
                return cell
            }
        })
    }
    
    func createSnapshot (grocs: [Grocs]) {
        var snapshot = NSDiffableDataSourceSnapshot <GrocListSections, Grocs>()
        snapshot.appendSections([.todo, .inputField, .done])
        snapshot.appendItems(grocs, toSection: .todo)
        snapshot.appendItems([Grocs(textFieldCell: "My Field")], toSection: .inputField)
        snapshot.appendItems(doneData, toSection: .done)
        datasource.apply(snapshot)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row != grocData.count {
                getDescription(index: indexPath.row)
            }
        } else if indexPath.section == 2 {
            showDescription(index: indexPath.row)
        } else {
            
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if indexPath.section == 0 {
            if indexPath.row != grocData.count {
                let deleteGrocAction = deleteGroc(indexPath, tableView: tableView)
                let updateGrocStatusAction = updateGrocStatus(indexPath, tableView: tableView)
                return UISwipeActionsConfiguration(actions: [deleteGrocAction, updateGrocStatusAction])
            }
            return UISwipeActionsConfiguration()
        } else if indexPath.section == 2 {
            let deleteGrocAction = deleteGroc(indexPath, tableView: tableView)
            let updateGrocStatusAction = updateGrocStatus(indexPath, tableView: tableView)
            return UISwipeActionsConfiguration(actions: [deleteGrocAction, updateGrocStatusAction])
        } else {
            return UISwipeActionsConfiguration(actions: [UIContextualAction()])
        }
    }
    
    func deleteGroc(_ indexPath: IndexPath, tableView: UITableView) -> UIContextualAction {
        guard let grocKey = self.grocKey else { return UIContextualAction() }
        let action = UIContextualAction(style: .destructive, title: "Delete") {(_, _, _)  in
            if indexPath.section == 0 {
                tableView.beginUpdates()
                guard let roomID = self.grocData[indexPath.row].roomID else { return }
                self.presenter?.deleteGroc(roomId: roomID, grocKey: grocKey)
                tableView.endUpdates()
            } else {
                tableView.beginUpdates()
                guard let roomID = self.doneData[indexPath.row].roomID else { return }
                self.presenter?.deleteGroc(roomId: roomID, grocKey: grocKey)
                tableView.endUpdates()
            }
        }
        action.image = #imageLiteral(resourceName: "delete")
        action.backgroundColor = UIColor(named: "fontColor3")
        return action
    }
    
    /****ADD BUTTON CLICK - GROC FROM TEXTFIELD**/
    func didTap(data: String) {
        if !data.trimmingCharacters(in: .whitespaces).isEmpty {
            buttonActionAddGroc(data: data)
        } else {
            let alert = UIAlertController(title: "Empty Text Field", message: "Please Enter Some Groc Item", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /****Keyboard Return**/
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /****MOVING BETWEEN Todo & DONE **/
    func updateGrocStatus(_ indexPath: IndexPath, tableView: UITableView) -> UIContextualAction {
        guard let grocKey = self.grocKey else { return UIContextualAction() }
        if indexPath.section == 0 {
            let action = UIContextualAction(style: .normal, title: "Mark As Done") {_, _, _ in
                if indexPath.row<self.grocData.count {
                    guard let currentUserId = self.grocData[indexPath.row].roomID else {return}
                    self.status = "0" // Moving To Done
                    self.presenter?.updateGrocStatus(roomId: currentUserId, status: self.status, grocKey: grocKey)
                }
            }
            action.image = #imageLiteral(resourceName: "Done")
            action.backgroundColor = UIColor(named: "fontColor3")
            return action
        } else {
            let action = UIContextualAction(style: .normal, title: "Mark As Undone") {(_, _, _)  in
                if indexPath.row<self.doneData.count {
                    guard let currentUserId = self.doneData[indexPath.row].roomID else {return}
                    self.status = "1" // Moving to Todo
                    self.presenter?.updateGrocStatus(roomId: currentUserId, status: self.status, grocKey: grocKey)
                }
            }
            action.image = #imageLiteral(resourceName: "Undone")
            action.backgroundColor = UIColor(named: "fontColor3")
            return action
        }
    }
    
    func getDescription(index: Int) {
        guard var description: String = grocData[index].grocDescription else { return }
        let alert = UIAlertController(title: grocData[index].grocName, message: "", preferredStyle: .alert)
        alert.addTextField {(textField: UITextField!) -> Void in
            textField.text = description
            textField.placeholder = "Enter Description"
        }
        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: {_ -> Void in
            description = (alert.textFields![0] as UITextField).text! as String
            guard let roomID = self.grocData[index].roomID else { return }
            guard let grocKey = self.grocKey else { return }
            self.presenter?.updateGrocDescription(roomId: roomID, description: description, grocKey: grocKey)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showDescription(index: Int) {
        let description: String = doneData[index].grocDescription ?? "No Dscription"
        let alert = UIAlertController(title: doneData[index].grocName, message: description, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    /**** PROTOCOL IMPLEMENTATION **/
    func setAssigneeName(name: String) {
        self.assigneeName = name
    }
    
    func error() {
        print("Some Error Occured")
    }
    
    func setupGrocView(grocList: [Grocs]) {
        let rawGrocData = grocList
        grocData = [Grocs]()
        doneData = [Grocs]()
        for groc in rawGrocData {
            if groc.status == "1" {
                grocData.append(groc)
            } else {
                doneData.append(groc)
            }
        }
        DispatchQueue.main.async {
            self.loadGrocData(pagination: true) { status in
                switch status {
                case .success:
                    self.createSnapshot(grocs: self.grocsToShow)
                case .failure:
                    print("Error Occured")
                }
            }
        }
    }
    func showLoader() {
        CommonFunctions.shared.showLoadingAnimation(view: self.view)
    }
    
    func hideLoader() {
        CommonFunctions.shared.hideLoadingAnimation()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.y
        if position > (grocTableView.contentSize.height - 50 - scrollView.frame.size.height) {
            guard !isPaginating else { return }
            self.grocTableView.tableFooterView = createSpinnerFooter()
            self.loadGrocData(pagination: true) { status in
                switch status {
                case .success:
                    DispatchQueue.main.async {
                        self.grocTableView.tableFooterView = nil
                        self.createSnapshot(grocs: self.grocsToShow)
                    }
                case .failure:
                    print("Data Failed")
                }
            }
        }
        if position < -50 {
            guard !isPaginating else { return }
            self.grocTableView.tableHeaderView = createSpinnerFooter()
            self.count = 0
            self.grocsToShow = []
            self.loadGrocData(pagination: true) { status in
                switch status {
                case .success:
                    DispatchQueue.main.async {
                        self.grocTableView.tableHeaderView = nil
                        self.createSnapshot(grocs: self.grocsToShow)
                    }
                case .failure:
                    print("Data Failed")
                }
            }
        }
    }
    private func createSpinnerFooter() -> UIView {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 100))
        let spinner = UIActivityIndicatorView()
        spinner.center = footerView.center
        footerView.addSubview(spinner)
        spinner.startAnimating()
        return footerView
    }
    
    func loadGrocData(pagination: Bool, completion: @escaping (Result<[Grocs], Error>) -> Void) {
        if pagination {
            self.isPaginating = true
        }
        if isPaginating == true {
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                for _ in 0..<20 where self.count < self.grocData.count {
                    self.grocsToShow.append(self.grocData[self.count])
                    self.count+=1
                }
                completion(.success(self.grocsToShow))
                self.hideLoader()
                self.isPaginating = false
            }
        }
    }
}

class GrocDataSource: UITableViewDiffableDataSource <GrocListSections, Grocs> {
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 || indexPath.section == 2 {
            return true
        } else {
            return false
        }
    }
}
