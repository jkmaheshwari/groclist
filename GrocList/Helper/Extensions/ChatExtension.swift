//
//  Extension.swift
//  GrocList
//
//  Created by Syed Asad on 11/03/2021.
//

import UIKit

extension ChatViewController: UITextFieldDelegate, UITableViewDelegate, ChatViewProtocol {
    func showLoader() {
        CommonFunctions.shared.showLoadingAnimation(view: self.view)
    }
    
    func hideLoader() {
        CommonFunctions.shared.hideLoadingAnimation()
    }
    
    func displayChat(chat: [Messages]) {
        self.msg = [Messages]()
        self.msg.append(contentsOf: chat)
        DispatchQueue.main.async {
            self.createSnapshot()
            if !self.msg.isEmpty {
                self.tableView.scrollToRow(at: IndexPath(row: (self.msg.count - 1), section: 0), at: .bottom, animated: true)
            }
        }
    }
	
	func loadMessages() {
		if !self.msg.isEmpty {
			self.tableView.scrollToRow(at: IndexPath(row: (self.msg.count - 1), section: 0), at: .bottom, animated: true)
		}
        hideLoader()
        self.createSnapshot()
	}
	
	func registerCellForChat() {
		let nibCell = UINib(nibName: "ViewRecievedCell", bundle: nil)
		tableView.register(nibCell, forCellReuseIdentifier: "receivedMessage")
		let nibCellSent = UINib(nibName: "ViewSentCell", bundle: nil)
		tableView.register(nibCellSent, forCellReuseIdentifier: "sentMessage")
	}
	
	// setTimeandDate
	func setTimeDateFormat (data: String) -> [String: String] {
		let getFormatter = DateFormatter()
		getFormatter.calendar = Calendar(identifier: .iso8601)
		getFormatter.locale = Locale(identifier: "es")
		getFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss zzzzz"
		let timeFormatter = DateFormatter()
		timeFormatter.timeStyle = .short
		timeFormatter.dateFormat = "hh:mm a"
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd-MMM-yyyy"
		print(data)
		if let dateTime = getFormatter.date(from: data) {
			let time = timeFormatter.string(from: dateTime)
			let date = dateFormatter.string(from: dateTime)
			print("Check: ", date)
			return ["time": time, "date": date]
		} else {
			return ["": ""]
		}
	}
	
	// Keyboard
	func hideKeyboardWhenTappedAround() {
		let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
		tap.cancelsTouchesInView = false
		self.tableView.addGestureRecognizer(tap)
	}
	
	@objc func dismissKeyboard() {
		view.endEditing(true)
	}
	
	@objc func keyboardWillShow(notification: Notification) {
		if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
			if self.view.frame.origin.y == 0 {
                
                self.view.frame = CGRect(x: 0, y: self.view.frame.height - keyboardSize.height, width: self.view.frame.width, height: keyboardSize.height - self.view.frame.height)
                self.view.layoutIfNeeded()
                self.tableView.scrollToRow(at: IndexPath(row: (self.msg.count - 1), section: 0), at: .bottom, animated: true)
                
			}
		}
	}
	
    @objc func keyboardWillHide(notification: Notification) {
        self.view.frame = originalViewFrame
        self.view.layoutIfNeeded()
    }
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		return true
	}
	
	// NavBar back Action
	@objc func leftHandAction() {
        self.view.endEditing(true)
        self.dismissKeyboard()
        presenter?.goBack()
	}
	
	func attributedNavigation() {
		navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.09019607843, green: 0.3568627451, blue: 0.6196078431, alpha: 1)
		navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor .white]
		navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(leftHandAction))
        navigationItem.leftBarButtonItem?.tintColor = .black
		navTitleAndImage(user?.name ?? "")
	}
	
	// Setting image and title on Navigation Bar
	func navTitleAndImage(_ title: String) {
		let titleLbl = UILabel()
		titleLbl.text = title
		titleLbl.textColor = UIColor.white
		titleLbl.font = UIFont.systemFont(ofSize: 16.0, weight: .semibold)
		
		let imageView = UIImageView()
		storageReference.child("ProfilePhotos").child(user?.userID ?? "").downloadURL { (url, _) in
            imageView.kf.indicatorType = .activity
            imageView.kf.setImage(with: url, placeholder: UIImage(named: "user"))
		}
		imageView.translatesAutoresizingMaskIntoConstraints = false
		imageView.widthAnchor.constraint(equalToConstant: 45).isActive = true
		imageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
		imageView.layer.cornerRadius = 20
		imageView.layer.masksToBounds = true
		imageView.layer.borderWidth = 0.2
		imageView.contentMode = UIView.ContentMode.scaleAspectFill
		imageView.backgroundColor = .white
		
		let titleView = UIStackView(arrangedSubviews: [imageView, titleLbl])
		titleView.contentMode = .center
		titleView.axis = .horizontal
		titleView.spacing = 5
		navigationItem.titleView = titleView
	}
	
    func createDatasource() {
        
        datasource = UITableViewDiffableDataSource(tableView: tableView, cellProvider: { (tableView, indexPath, message) in
            
            let timeMessage = self.setTimeDateFormat(data: message.time!)
            if message.fromID != self.fromID {
                guard let cellReceivedMessage = tableView.dequeueReusableCell(withIdentifier: "receivedMessage", for: indexPath) as? TableViewCellReceivedMessages else {
                    return UITableViewCell()
                }
                cellReceivedMessage.configure(msg: message, time: timeMessage["time"] ?? "")
                
                self.storageReference.child("ProfilePhotos").child(self.user?.userID ?? "").downloadURL { (url, _)in
                    cellReceivedMessage.configProfileImage(url: url!)
                }
                return cellReceivedMessage
            } else {
                guard let cellSentMessage = tableView.dequeueReusableCell(withIdentifier: "sentMessage", for: indexPath) as? TableViewCellSentMessages else {
                    return UITableViewCell()
                }
                cellSentMessage.configure(message: message, time: timeMessage["time"] ?? "")
                return cellSentMessage
            }
            
        })
    }
    
    func createSnapshot() {
        var snapshot = NSDiffableDataSourceSnapshot <ChatSections, Messages>()
        snapshot.appendSections([.messages])
        snapshot.appendItems(msg)
        datasource.apply(snapshot, animatingDifferences: false)
    }
}
