//
//  MainViewExtension.swift
//  GrocList
//
//  Created by Jatesh Kumar on 11/03/2021.
//

import UIKit
import Firebase
import Kingfisher

extension MainViewController: UITableViewDelegate, MainViewProtocol {
    
    func updateView(users: [User]) {
        self.friendList.removeAll()
        friendList.append(contentsOf: users)
        DispatchQueue.main.async {
            self.createSnapshot()
            self.hideLoader()
            if self.friendList.count > 0 {
                self.labelNoGrocsFound.isHidden = true
            } else {
                self.labelNoGrocsFound.isHidden = false
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeader = UILabel()
        sectionHeader.text = "Add New Friends"
        return sectionHeader
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let secondUserID = friendList[indexPath.row].userID, let currentUserID = self.currentUserID else { return }
        presentor?.secondUser = friendList[indexPath.row]
        presentor?.goToGrocRoom(currentUserID: currentUserID, secondUserID: secondUserID)
    }
    
    // MARK: - Diffable Datasource
    func createDatasource () {
        datasource = UITableViewDiffableDataSource(tableView: tableView, cellProvider: { (tableView, indexPath, userData) in
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as? MainUserCell else { return UITableViewCell() }
            cell.imageViewProfilePicture.image = nil
            cell.labelName.text = userData.name
            GrocDbManager.shared.getProfilePicture(userID: userData.userID ?? "") {(status) in
                switch status {
                case .success(let url):
                    cell.imageViewProfilePicture.kf.indicatorType = .activity
                    cell.imageViewProfilePicture.kf.setImage(with: url, placeholder: UIImage(named: "user"))
                case .failure(let error):
                    print("Storage Error: ", error)
                }
            }
            guard let userId = self.currentUserID else { return UITableViewCell()}
            GrocDbManager.shared.grocItemsUpdated(currentUserID: userId, secondUserID: userData.userID, eventType: .value) { (status) in
                switch status {
                case .success(let count):
                    guard let total = count["total"], let done = count["done"], let todo = count["todo"], let unread = count["unread"] as? String else { return }
                    
                    guard let timestamp = count["time"] as? String else { return }
                    cell.labelGrocStatus.text = "Items: \(total) | Done: \(done) | Pending: \(todo)"
                    if unread != "0" {
                        cell.buttonUnreadCount.isHidden = false
                        cell.buttonUnreadCount.setTitle(String(unread), for: .normal)
                    }
                    if unread == "0" {
                        cell.buttonUnreadCount.isHidden = true
                    }
                    let dateTime = CommonFunctions.shared.setTimeDateFormat(data: timestamp)
                    cell.labelTime.text = dateTime["time"]
                case .failure(let error):
                    print("Error: ", error)
                }
            }
            return cell
        })
    }
    
    func createSnapshot() {
        var snapshot = NSDiffableDataSourceSnapshot <MainViewSections, User>()
        snapshot.appendSections([.first])
        snapshot.appendItems(friendList)
        datasource.apply(snapshot, animatingDifferences: false)
    }
}
