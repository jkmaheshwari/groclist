//
//  ExtensionGrocDbManager.swift
//  GrocList
//
//  Created by Jatesh Kumar on 22/04/2021.
//

import Foundation
import Firebase
import FirebaseUI
import FirebaseStorage

extension GrocDbManager {
    /****GROC VIEW CONTROLLER**/
    func setGrocItem(data: Grocs, roomKey: String?) {
        let currentTime = NSDate().description
        var grocData: [String: String?]
        grocData = [ "name": data.grocName,
                     "description": data.grocDescription,
                     "assigneeID": data.grocAssigneeID,
                     "status": data.status,
                     "timestamp": currentTime,
                     "seenStatus": "0" ]
        guard let roomKey = roomKey else { return }
        self.dbReference.child("GrocList").child(roomKey ).child("grocs").childByAutoId().updateChildValues(grocData as [AnyHashable: Any])
    }
    
    func getTodoGrocItems(grocKey: String, seenStatus: String, completion: @escaping (Result<[Grocs], Error>) -> Void) {
        dbReference.child("GrocList").child(grocKey).child("grocs").getData { (error, snapshot) in
            if let error = error {
                completion(.failure(error))
                return
            } else if let newSnapshot = snapshot.children.allObjects as? [DataSnapshot] {
                print("GetToDoList Count: ", self.grocData.count)
                DispatchQueue.main.async {
                    self.grocData = [Grocs]()
                    for snap in newSnapshot {
                        if var dictionary = snap.value as? [String: AnyObject] {
                            print("Jatesh: ", dictionary)
                            dictionary["id"] = snap.key as AnyObject
                            self.grocData.append(Grocs(data: dictionary))
                            if dictionary["assigneeID"] as? String != Auth.auth().currentUser?.uid && seenStatus == "change" {
                                self.dbReference.child("GrocList").child(grocKey).child("grocs").child(snap.key).updateChildValues(["seenStatus": "1"])
                            }
                        }
                    }
                    completion(.success(self.grocData))
                }
            }
        }
    }
    
    func grocItemChanged(eventType: DataEventType, grocKey: String, completion: @escaping (Result<[Grocs], Error>) -> Void ) {
        self.dbReference.child("GrocList").child(grocKey).observe(eventType, with: { _ in
            if eventType == .childAdded {
                self.seenStatus = "change"
            }
            self.getTodoGrocItems(grocKey: grocKey, seenStatus: self.seenStatus) {(data) in
                self.grocData = [Grocs]()
                switch data {
                case .success(let data):
                    data.forEach({ (data) in
                        self.grocData.append(data)
                    })
                    completion(.success(self.grocData))
                case .failure(let error):
                    print("Error: ", error)
                    completion(.failure(error))
                }
            }
            self.seenStatus = "unchange"
        })
    }
    
    func getAssigneeName(uid: String, completion: @escaping (Result<String, Error>) -> Void) {
        self.dbReference.child("users").getData { (_, snapshot) in
            if let snap = snapshot.children.allObjects as? [DataSnapshot] {
                DispatchQueue.main.async {
                    for data in snap where  data.key == uid {
                        if let dictionary = data.value as? [String: AnyObject] {
                            guard let name: String = dictionary["name"] as? String else { return }
                            completion(.success(name))
                        }
                    }
                }
            }
        }
    }
    
    func updateGrocStatus (roomId: String, status: String, grocKey: String?) -> Bool {
        guard let grocKey = grocKey else {
            return false
        }
        self.dbReference.child("GrocList").child(grocKey).child("grocs").observe(.childAdded, with: {(snapshot) in
            if roomId == snapshot.key {
                self.dbReference.child("GrocList").child(grocKey).child("grocs").child(roomId).updateChildValues(["status": status])
            }
        }, withCancel: nil)
        return true
    }
    
    func updateGrocDesc (roomId: String, description: String, grocKey: String?) -> Bool {
        guard let grocKey = grocKey else {
            return false
        }
        self.dbReference.child("GrocList").child(grocKey).child("grocs").observe(.childAdded, with: {(snapshot) in
            if roomId == snapshot.key {
                self.dbReference.child("GrocList").child(grocKey).child("grocs").child(roomId).updateChildValues(["description": description])
            }
        }, withCancel: nil)
        return true
    }
    
    func deleteGroc (roomId: String, grocKey: String?) -> Bool {
        guard let grocKey = grocKey else {
            return false
        }
        self.dbReference.child("GrocList").child(grocKey).child("grocs").child(roomId).removeValue()
        return true
    }
}
