//
//  CommonFunctions.swift
//  GrocList
//
//  Created by Jatesh Kumar on 31/03/2021.
//

import UIKit
import Lottie

class CommonFunctions {
    static let shared = CommonFunctions()
    var animationView = AnimationView()
    /****FORMAT DATE & TIME**/
    func setTimeDateFormat (data: String) -> [String: String] {
        let getFormatter = DateFormatter()
        getFormatter.calendar = Calendar(identifier: .iso8601)
        getFormatter.locale = Locale(identifier: "es")
        getFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss zzzzz"
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .short
        timeFormatter.dateFormat = "hh:mm a"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        if let dateTime = getFormatter.date(from: data) {
            let time = timeFormatter.string(from: dateTime)
            let date = dateFormatter.string(from: dateTime)
            return ["time": time, "date": date]
        } else {
            return ["": ""]
        }
    }
    
    func showLoadingAnimation(view: UIView) {
        animationView.isHidden = false
        animationView.animation = Animation.named("53175-ball-spinner")
        animationView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        animationView.center = view.center
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        animationView.play()
        view.addSubview(animationView)
    }
    
    func hideLoadingAnimation() {
        animationView.stop()
        animationView.isHidden = true
    }
}
