//
//  ChatLogViewController.swift
//  GrocList
//
//  Created by Syed Asad on 24/02/2021.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseUI

class ChatViewController: UIViewController {
	
	var roomCount: Int = 1
	var isChatRoomCreated: Bool = false
	var dbReference = DatabaseReference.init()
	var user: User?
	let time = NSDate().description
	let fromID = Auth.auth().currentUser?.uid
	var msg = [Messages]()
	var chatroomKey: String?
	var storageReference = Storage.storage().reference()
    var presenter: ChatPresenterProtocol?
    var originalViewFrame = CGRect()
    var datasource: UITableViewDiffableDataSource <ChatSections, Messages>!
	// OUTLETS
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var fieldTypeMessage: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.dbReference = Database.database().reference()
		
		self.tableView.delegate = self
		self.tableView.dataSource = datasource
		fieldTypeMessage.delegate = self
        guard let chatKey = chatroomKey else { return }
        presenter?.getMessages(chatRoomID: chatKey)
        
		NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
		
		showLoader()
		registerCellForChat()
		attributedNavigation()
        originalViewFrame = self.view.frame
		self.hideKeyboardWhenTappedAround()
        self.createDatasource()
        self.createSnapshot()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		self.loadMessages()
		print("ChatRoom Key: ", chatroomKey ?? "")
		if !self.msg.isEmpty {
			self.tableView.scrollToRow(at: IndexPath(row: (self.msg.count - 1), section: 0), at: .bottom, animated: true)
		}
		self.createSnapshot()
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	@IBAction func buttonActionSendMessage(_ sender: Any) {
		
        guard let currentID = Auth.auth().currentUser?.uid, let msg = fieldTypeMessage.text, let chatKey = chatroomKey else {
			return
		}
		if fieldTypeMessage.text != nil {
			print(fieldTypeMessage.text ?? "")
			let values = ["fromID": currentID,
						  "msg": msg,
						  "time": time] as [String: String]
			DispatchQueue.main.async {
                self.presenter?.sendMessage(chatKey: chatKey, message: values)
			}
		}
		fieldTypeMessage.text = ""
	}
}

public enum ChatSections {
    case messages
}
