//
//  TableViewCellSentMessages.swift
//  GrocList
//
//  Created by Syed Asad on 01/03/2021.
//

import UIKit

class TableViewCellSentMessages: UITableViewCell {
	
	@IBOutlet weak var viewSentMessages: UIView!
	@IBOutlet weak var labelSentMessages: UILabel!
	@IBOutlet weak var labelTimeSent: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(message: Messages, time: String) {
        labelSentMessages.text = message.msg
        viewSentMessages.layer.cornerRadius = 8.0
        viewSentMessages.layer.borderWidth = 0.1
        labelTimeSent.text = time
    }
}
