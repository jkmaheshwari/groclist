//
//  FriendListViewController.swift
//  GrocList
//
//  Created Jatesh Kumar on 05/04/2021.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import Firebase

class FriendListViewController: UIViewController {

	var presenter: FriendListPresenterProtocol?
    var friends = [User]()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelNoFriends: UILabel!
    var currenUserID: String = ""
    var grocKey: String = ""
    
    var datasource: UITableViewDiffableDataSource <AddFriendsTableViewSections, User>!
    
    deinit {
        print("deinit FriendListViewController")
    }

	override func viewDidLoad() {
        super.viewDidLoad()
        self.showLoader()
        labelNoFriends.isHidden = true
        guard let userID = Auth.auth().currentUser?.uid else { return }
        self.currenUserID = userID
        presenter?.getMyFriendList(myUserID: userID)
        tableView.delegate = self
        register()
        createDatasource()
        createSnapshot()
    }
    
    func register() {
        let nib = UINib(nibName: "FriendListCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "friendListCell")
    }
}

extension FriendListViewController: UITableViewDelegate {
    func createDatasource () {
        datasource = UITableViewDiffableDataSource(tableView: tableView, cellProvider: { (tableView, indexPath, friendList) in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "friendListCell", for: indexPath) as? FriendListCell else { return UITableViewCell() }
            
            cell.imageViewProfilePicture.image = nil
            GrocDbManager.shared.getProfilePicture(userID: friendList.userID ?? "") {(status) in
                switch status {
                case .success(let url):
                    cell.imageViewProfilePicture.kf.setImage(with: url, placeholder: UIImage(named: "user"))
                case .failure(let error):
                    print("Storage Error: ", error)
                }
            }
            cell.configure(user: friendList)
            return cell
        })
    }
    
    func createSnapshot () {
        var snapshot = NSDiffableDataSourceSnapshot<AddFriendsTableViewSections, User>()
        snapshot.appendSections([.first])
        snapshot.appendItems(friends)
        datasource.apply(snapshot, animatingDifferences: false)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let secondUserID = datasource.itemIdentifier(for: indexPath)?.userID else { return }
        presenter?.secondUser = datasource.itemIdentifier(for: indexPath)
        presenter?.checkGrocRoom(currentUserId: self.currenUserID, secondUserId: secondUserID)
    }
}

extension FriendListViewController: FriendListViewProtocol {
    func updateView(friends: [User]) {
        self.friends = friends
        DispatchQueue.main.async {
            self.createSnapshot()
            if friends.count > 0 {
                self.labelNoFriends.isHidden = true
            } else {
                self.labelNoFriends.isHidden = false
            }
            self.hideLoader()
        }
    }
    
    func showLoader() {
        CommonFunctions.shared.showLoadingAnimation(view: self.view)
    }
    
    func hideLoader() {
        CommonFunctions.shared.hideLoadingAnimation()
    }
}

public enum AddFriendsTableViewSections {
    case first
}
