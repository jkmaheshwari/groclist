//
//  FriendRequestsViewController.swift
//  GrocList
//
//  Created Jatesh Kumar on 01/04/2021.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import Firebase

class FriendRequestsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelNoFriendRequests: UILabel!
    var presenter: FriendRequestsPresenterProtocol?
    var friendRequests = [User]()
    var currentUserID: String = ""
    var datasource: UITableViewDiffableDataSource <AddFriendsTableViewSections, User>!
    deinit {
        print("deinit FriendRequestsViewController")
    }

	override func viewDidLoad() {
        super.viewDidLoad()
        self.showLoader()
        tableView.delegate = self
        register()
        createDatasource()
        createSnapshot()
        
        guard let userID = Auth.auth().currentUser?.uid else { return }
        self.currentUserID = userID
        presenter?.getAllFriendRequests(roomID: currentUserID)
    }
    
    func register() {
        let nib = UINib(nibName: "RequestCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "requestCell")
    }
}

extension FriendRequestsViewController: UITableViewDelegate {
    func createDatasource () {
        datasource = UITableViewDiffableDataSource(tableView: tableView, cellProvider: { (tableView, _, requests) in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "requestCell") as? RequestCell else { return UITableViewCell() }
            GrocDbManager.shared.getProfilePicture(userID: requests.userID ?? "") {(status) in
                switch status {
                case .success(let url):
                    cell.imageViewProfilePicture.kf.setImage(with: url, placeholder: UIImage(named: "user"))
                case .failure(let error):
                    print("Storage Error: ", error)
                }
            }
            cell.delegate = self
            cell.configure(user: requests)
            return cell
        })
    }
    
    func createSnapshot() {
        var snapshot = NSDiffableDataSourceSnapshot<AddFriendsTableViewSections, User>()
        snapshot.appendSections([.first])
        snapshot.appendItems(friendRequests)
        datasource.apply(snapshot, animatingDifferences: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        .leastNormalMagnitude
    }
}

extension FriendRequestsViewController: FriendRequestsViewProtocol, RequestCellDelegate {
    func setupRequests(requests: [User]) {
        self.friendRequests = requests
        DispatchQueue.main.async {
            self.createSnapshot()
            if self.friendRequests.count > 0 {
                self.labelNoFriendRequests.isHidden = true
            } else {
                self.labelNoFriendRequests.isHidden = false
            }
            self.hideLoader()
        }
    }
    
    func friendRequest(accept: Bool, userID: String) {
        presenter?.changeFriendRequestStatus(accept: accept, userID: userID, roomKey: currentUserID)
    }
    
    func showLoader() {
        CommonFunctions.shared.showLoadingAnimation(view: self.view)
    }
    
    func hideLoader() {
        CommonFunctions.shared.hideLoadingAnimation()
    }
}
