//
//  ProfileViewViewController.swift
//  GrocList
//
//  Created Jatesh Kumar on 23/04/2021.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var imageViewProfilePicture: UIImageView!
    @IBOutlet weak var labelFullName: UILabel!
    @IBOutlet weak var labelEmailAddress: UILabel!
    @IBOutlet weak var buttonEditProfileOutlet: UIButton!
    var presenter: ProfileViewPresenterProtocol?
    var profileImage = UIImage()
    var fullName = ""
    var email = ""
    var userID = ""
    
    deinit {
        print("deinit ProfileViewViewController")
    }

	override func viewDidLoad() {
        super.viewDidLoad()
        buttonEditProfileOutlet.layer.cornerRadius = 10
        imageViewProfilePicture.image = profileImage
        labelFullName.text = fullName
        labelEmailAddress.text?.append(email)
        setUpNavigation()
    }
    
    @IBAction func buttonEditProfileAction(_ sender: UIButton) {
        let editProfile = SignupRouter.createModule()
        editProfile.userID = userID
        navigationController?.pushViewController(editProfile, animated: true)
    }
}

extension ProfileViewController {
    func setUpNavigation() {
        navigationItem.title = "My Profile"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .done, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem?.tintColor = .black
    }
    
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
}

extension ProfileViewController: ProfileViewViewProtocol {

     func showLoader() {
        DispatchQueue.main.async {

        }
    }
    
    func hideLoader() {
        DispatchQueue.main.async {
            
        }
    }
}
