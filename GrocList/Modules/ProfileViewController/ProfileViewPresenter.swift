//
//  ProfileViewPresenter.swift
//  GrocList
//
//  Created Jatesh Kumar on 23/04/2021.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class ProfileViewPresenter {

    weak private var view: ProfileViewViewProtocol?
    var interactor: ProfileViewInteractorInputProtocol?
    private let router: ProfileViewWireframeProtocol

    init(interface: ProfileViewViewProtocol, interactor: ProfileViewInteractorInputProtocol?, router: ProfileViewWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    deinit {
        print("deinit ProfileViewPresenter")
    }

    func viewDidLoad() {

    }
}

extension ProfileViewPresenter: ProfileViewPresenterProtocol {

}

extension ProfileViewPresenter: ProfileViewInteractorOutputProtocol {

}
