//
//  ProfileViewInteractor.swift
//  GrocList
//
//  Created Jatesh Kumar on 23/04/2021.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class ProfileViewInteractor {

    weak var presenter: ProfileViewInteractorOutputProtocol?

    deinit {
        print("deinit ProfileViewInteractor")
    }
}

extension ProfileViewInteractor: ProfileViewInteractorInputProtocol {

}
