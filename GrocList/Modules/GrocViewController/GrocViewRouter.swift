//
//  GrocViewRouter.swift
//  GrocList
//
//  Created by Jatesh Kumar on 30/03/2021.
//

import Foundation
import UIKit

final class GrocViewRouter {
    
    weak var viewController: UIViewController?
    
    static func createModule(grockKey: String?, secondUser: User?) -> GrocViewController {
        let view = grocStoryboard.instantiateViewController(identifier: "GrocViewController") as? GrocViewController
        
        let presenter: GrocViewPresenterProtocol & GrocViewInteractorOutputProtocol = GrocViewPresenter()
        let interactor: GrocViewInteractorInputProtocol = GrocViewInteractor()
        let router = GrocViewRouter()
        
        view?.presenter = presenter
        presenter.view = view
        interactor.presenter = presenter
        presenter.interactor = interactor
        router.viewController = view
        presenter.router = router
        
        view?.grocKey = grockKey
        view?.secondUser = secondUser
        return view!
    }
    static var grocStoryboard: UIStoryboard { return UIStoryboard(name: "Main", bundle: Bundle.main) }
}

extension GrocViewRouter: GrocViewWireframeProtocol {
    func openChatRoomModule(grocKey: String, secondUser: User) {
        let chatController = ChatRouter.createModule(chatKey: grocKey, user: secondUser)
        self.viewController?.navigationController?.pushViewController(chatController, animated: true)
    }
    
    func goBack() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
}
