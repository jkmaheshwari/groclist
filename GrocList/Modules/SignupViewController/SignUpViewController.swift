//
//  ViewController.swift
//  GrocList
//
//  Created by Shahzaib Iqbal Bhatti on 2/22/21.
//

import UIKit
import Photos
import Firebase

class SignUpViewController: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var fieldName: UITextField!
    @IBOutlet weak var fieldEmailAddress: UITextField!
    @IBOutlet weak var fieldPassword: UITextField!
    @IBOutlet weak var fieldConfirmPassword: UITextField!
    @IBOutlet weak var buttonOutletSignUp: UIButton!
    @IBOutlet weak var buttonSelectImage: UIButton!
    @IBOutlet weak var viewImagePicker: UIView!
    
    var imagePickerController = UIImagePickerController()
    var imageUrl: URL?
    var userID: String = ""
    var presenter: SignupViewToPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePickerController.delegate = self
        fieldName.delegate = self
        fieldPassword.delegate = self
        fieldConfirmPassword.delegate = self
        fieldEmailAddress.delegate = self
        self.setUpView()
        self.checkRegisterationStatus()
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func buttonActionSelectImage(_ sender: UIButton) {
        self.imagePickerController.sourceType = .photoLibrary
        self.present(self.imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func buttonActionSignIn(_ sender: Any) {
        switchToSignInViewController()
    }
    
    @IBAction func buttonActionSignUp(_ sender: Any) {
        let email = fieldEmailAddress.text!
        let password = fieldConfirmPassword.text!
        let name = fieldName.text!
        guard let imageURL = imageUrl else { return }
        if userID != "" {
            presenter?.updateUser(uid: userID, password: password, name: name, imageUrl: imageURL)
        } else {
            if name != "", email != "", password != "" {
                if password == fieldPassword.text {
                    presenter?.signupUser(email: email, password: password, name: name, imageUrl: imageURL)
                } else {
                    callAlert("Password Incorrect!", "Please provide correct password", action: UIAlertAction(title: "Ok", style: .default, handler: nil))
                }
            } else {
                callAlert("Empty Fields!", "Please fill all the fields", action: UIAlertAction(title: "Ok", style: .default, handler: nil))
            }
        }
    }
}

extension SignUpViewController: SignupPresenterToView {
    func checkUserUpdated(status: Bool) {
        if status {
            let title = "Update Successful"
            let description = ""
            self.alert(title: title, description: description)
        } else {
            let title = "Update Failed"
            let description = "Some internal error occured, please try again!"
            self.alert(title: title, description: description)
        }
    }
    
    func checkUserCreated(status: Bool) {
        if status {
            let title = "Registration Successful"
            let description = "Please login to get started!"
            self.alert(title: title, description: description)
        } else {
            let title = "Registration Failed"
            let description = "Some internal error occured, please try again!"
            self.alert(title: title, description: description)
        }
    }
    
    func alert(title: String, description: String) {
        self.callAlert(title, description, action: UIAlertAction(title: "Ok", style: .default, handler: { (_: UIAlertAction) in
            if title == "Registration Successful" {
                self.switchToSignInViewController()
            }
            if title == "Update Successful"{
                self.switchToProfileViewController()
            }
        }))
    }
    
    func setUpView() {
        setCornerRadius(field: fieldName)
        setCornerRadius(field: fieldEmailAddress)
        setCornerRadius(field: fieldPassword)
        setCornerRadius(field: fieldConfirmPassword)
        
        buttonOutletSignUp.layer.cornerRadius = 10.0
        buttonOutletSignUp.layer.borderWidth = 0.1
        buttonSelectImage.layer.cornerRadius = buttonSelectImage.frame.width/2
        buttonSelectImage.layer.borderWidth = 0.3
        buttonSelectImage.contentMode = UIView.ContentMode.scaleAspectFit
        buttonSelectImage.layer.masksToBounds = true
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .done, target: self, action: #selector(goBack))
        navigationItem.title = "Update Profile"
        navigationItem.leftBarButtonItem?.tintColor = .black
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    func checkRegisterationStatus() {
        if userID != "" {
            GrocDbManager.shared.getProfilePicture(userID: userID) { status in
                switch status {
                case .success(let url):
                    self.imageUrl = url
                    self.buttonSelectImage.kf.setImage(with: url, for: .normal)
                case .failure(let error):
                    print(error)
                }
            }
            fieldName.text = Auth.auth().currentUser?.displayName
            fieldEmailAddress.text = Auth.auth().currentUser?.email
            fieldPassword.isHidden = true
            fieldConfirmPassword.isHidden = true
            fieldEmailAddress.isEnabled = false
            buttonOutletSignUp.setTitle("Update", for: .normal)
        } else {
            self.attributedPlaceholder(fieldView: fieldName, text: "Full Name")
            self.attributedPlaceholder(fieldView: fieldEmailAddress, text: "Email Address")
            self.attributedPlaceholder(fieldView: fieldEmailAddress, text: "Email Address")
            self.attributedPlaceholder(fieldView: fieldPassword, text: "Enter 6 digits long Password")
            self.attributedPlaceholder(fieldView: fieldConfirmPassword, text: "Confirm Password")
        }
    }
}
