//
//  MainPresenter.swift
//  GrocList
//
//  Created by Jatesh Kumar on 30/03/2021.
//

import Foundation
import UIKit
import Firebase

class MainPresenter: MainPresenterProtocol {
    var secondUser: User?
    
    func goToGrocRoom(currentUserID: String, secondUserID: String) {
        interactor?.goToGrocRoom(currentUserID: currentUserID, secondUserID: secondUserID)
    }
    
    func goToLoginViewController() {
        router?.goToLoginViewController()
    }
    
    var view: MainViewProtocol?
    var interactor: MainInteractorInputProtocol?
    var router: MainWireframeProtocol?
    
    func showUsers(currentUserID: String) {
        interactor?.getUsers(roomID: currentUserID)
    }
}
extension MainPresenter: MainInteractorOutputProtocol {
    func getGrocRoom(grocKey: String) {
        router?.goToGrocViewController(grocKey: grocKey, secondUser: secondUser)
    }
    
    func fetchedFriends(users: [User]) {
        view?.updateView(users: users)
    }
}
