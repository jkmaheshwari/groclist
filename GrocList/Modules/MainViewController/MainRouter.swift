//
//  MainRouter.swift
//  GrocList
//
//  Created by Jatesh Kumar on 30/03/2021.
//

import Foundation
import UIKit

final class MainRouter {
    
    var viewController: UIViewController?
    
    static func createModule() -> MainViewController {
        
        let view = mainstoryboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
        
        let presenter: MainPresenterProtocol & MainInteractorOutputProtocol = MainPresenter()
        let interactor: MainInteractorInputProtocol = MainInteractor()
        let router = MainRouter()
        
        view?.presentor = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        router.viewController = view
        
        return view!
        
    }
    static var mainstoryboard: UIStoryboard { return UIStoryboard(name: "Main", bundle: nil) }
}

extension MainRouter: MainWireframeProtocol {
    func goToGrocViewController(grocKey: String, secondUser: User?) {
        let grocViewController = GrocViewRouter.createModule(grockKey: grocKey, secondUser: secondUser)
        self.viewController?.navigationController?.pushViewController(grocViewController, animated: true)
    }
    
    func goToLoginViewController() {
        let login = LoginRouter.createModule()
        self.viewController?.navigationController?.pushViewController(login, animated: true)
    }
}
