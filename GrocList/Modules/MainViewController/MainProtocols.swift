//
//  MainProtocols.swift
//  GrocList
//
//  Created by Jatesh Kumar on 30/03/2021.
//

import Foundation
import UIKit

protocol MainWireframeProtocol: class {
    static func createModule () -> MainViewController
    func goToLoginViewController()
    func goToGrocViewController(grocKey: String, secondUser: User?)

}

protocol MainPresenterProtocol: class {
    var view: MainViewProtocol? { get set }
    var interactor: MainInteractorInputProtocol? { get set }
    var secondUser: User? {get set}
    
    var router: MainWireframeProtocol? { get set }
    func showUsers(currentUserID: String)
    func goToLoginViewController()
    func goToGrocRoom(currentUserID: String, secondUserID: String)
}

protocol MainInteractorInputProtocol: class {
    var presenter: MainInteractorOutputProtocol? { get set }
    func getUsers(roomID: String)
    func goToGrocRoom(currentUserID: String, secondUserID: String)
}

protocol MainInteractorOutputProtocol: class {
    func fetchedFriends(users: [User])
    func getGrocRoom(grocKey: String)
}

protocol MainViewProtocol: class {
    func updateView(users: [User])
}
